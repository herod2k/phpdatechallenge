<?php
class MyDate {

  /**
  * Diff method calculates the difference between two dates using the Julian date format to approach the calculation. 
  *
  * @param string $start start date in YYYY/MM/DD format
  * @param string $end end date in YYYY/MM/DD format
  * @return object
  */
  public static function diff($start, $end) {

    $start=self::stringToDateArray($start);
    $end=self::stringToDateArray($end);
    $start_jd=self::gregorianToJD($start['day'],$start['month'],$start['year']);
    $end_jd=self::gregorianToJD($end['day'],$end['month'],$end['year']);

    $total_days = abs($end_jd - $start_jd);
    $years = (int)floor($total_days / 365);
    $months = (int)floor(($total_days - (365 * $years)) / 29.5);
    $days = abs($end['day'] - $start['day']);
    $invert = ($end_jd < $start_jd);

    return (object)array(
      'years' => $years,
      'months' => $months,
      'days' => $days,
      'total_days' => $total_days,
      'invert' => $invert
    );
  }

  /**
  * stringToDateArray method converts a string date into an array containing the date information
  *
  * @param string $string date in YYYY/MM/DD format
  * @return array
  */
  private function stringToDateArray($string){
    $date=explode('/', $string);
    return array(
      'year'=>(int)$date[0],
      'month'=>(int)$date[1],
      'day'=>(int)$date[2]
    );
  }

  /**
  * gregorianToJD method converts the Gregorian date into a Julian date
  *
  * @param integer $day
  * @param integer $month
  * @param integer $year
  * @return integer conaining the Julian Date
  */
  private function gregorianToJD($day, $month, $year){
    if ($month == 1 or $month == 2){
      $year--;
      $month = $month + 12;
    }

    $a = floor($year / 100);
    $b = 2 - $a + floor($a / 4);

    $c = floor(365.25 * $year);
    $d = floor(30.6001 * ( $month + 1 ));
    
    $date = (int)round($b + $c + $d + $day + 1720994.50);
    
    return $date;
  }
}